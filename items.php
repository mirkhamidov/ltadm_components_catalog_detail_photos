<?
$items=array(

	"id_lt_catalog_items" => array(
        "desc" => "к товару",
        "type" => "select_from_tables_as_tree",
		"size" => 10,
		"width" => "100%",
		"links" => array (
			array(
				"desc" => "Раздел",
				"table" => "lt_catalog_chapters",
				"key_field" => "id_lt_catalog_chapters",
				"fields" => array("name"),
				"show_field" => "%1",
				"order" => array ("orders" => "ASC","name" => "ASC"),
				"condition" => "",
			),
			array(
				"desc" => "Товары",
				"table" => "lt_catalog_items",
				"key_field" => "id_lt_catalog_items",
				"parent" => "id_lt_catalog_chapters",
				"fields" => array("name"),
				"show_field" => "%1",
				"order" => array ("name" => "ASC"),
			),
		),
		"select_on_edit" => true,
	),

  "name" => array(
         "desc" => "Название фото",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
         ),
       ),
  "photo" => array
  (
    "type"                  => "processed_image",
    "file_is"               => "processed_image",
    "desc"                  => "Фотография",
    "full_desc"             => "Только изображения форматов JPEG, GIF и PNG",
    "store_source_name"     => true,          // сохранять ли имя загруженного файла, которое тот имеет на компьютере пользователя?
    "store_dimensions"      => true,          // хранить ли в БД размеры картинки, или определять их из файла (false)
    "check_file"            => true,          // проверять ли при отображении картинки существование файла, или доверять информации из БД (false)
    "check_dimensions"      => true,          // проверять ли при отображении картинки её размеры, или доверять информации из БД (false)
    "lt_show_system"        => true,          // разрешает отображать lt_source и lt_preview
    "lt_delete_files"       => true,          // разрешает возможность удалять файлы картинок для данной записи
    "lt_allow_name_change"  => false,          // разрешает загружать файл под именем, указанным пользователем
    "lt_preview"            => "preview",        // индекс картинки, которая считается картинкой для предварительного просмотра
    "lt_allow_remote_files" => true,

    "images" => array
    (
      "preview" => array
		  (
			"prefix"  => "small_",
			"target_type" => 2,
			"transforms" => array
			(
			  "resize" => array(
					"w"=> 100,
					#"h"=>85,
					"backgroundColor" => array(255,255,255)
			  ),
			),
			"quality" => 100
		  ),
		  "show" => array
		  (
			"prefix"  => "show_",
			"target_type" => 2,
			"transforms" => array
			(
				"resize" => array(
					"w"=> 500,
					#"h"=>0,
					"backgroundColor" => array(255,255,255)
				),
			),
			"quality" => 100
		  ),

		  "source" => array
		  (
			"prefix"  => "src_",
			"target_type" => 2,
			"quality" => 100
		  ),


    ),
    "select_on_edit" => true,
  ),


);
?>