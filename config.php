<?
$config=array(
	"name" => "Детализированные фотографии",
	"status" => "system",
	"windows" => array(
		"create" => array("width" => 600,"height" => 350),
		"edit" => array("width" => 550,"height" => 400),
		"list" => array("width" => 800,"height" => 500),
	),
	"right" => array("admin","#GRANTED"),
	"main_table" => "lt_catalog_detail_photos",
	"list" => array(
		"id_lt_catalog_items" => array("isLink" => true),
		"name" => array("isLink" => true),
		"photo" => array("isLink" => true,"disabledSort" => true),
	),
	"select" => array(
		"max_perpage" => 100,
		"default_orders" => array(
			array("name" => "ASC"),
		),
		"default" => array(
			"id_lt_catalog_items" => array(
				"desc" => "к товару",
				"type" => "select_from_tables_as_tree",
				"size" => 10,
				"width" => "100%",
				"links" => array (
					array(
						"desc" => "Раздел",
						"table" => "lt_catalog_chapters",
						"key_field" => "id_lt_catalog_chapters",
						"fields" => array("name"),
						"show_field" => "%1",
						"order" => array ("orders" => "ASC","name" => "ASC"),
						"condition" => "",
					),
					array(
						"desc" => "Товары",
						"table" => "lt_catalog_items",
						"key_field" => "id_lt_catalog_items",
						"parent" => "id_lt_catalog_chapters",
						"fields" => array("name"),
						"show_field" => "%1",
						"order" => array ("name" => "ASC"),
					),
				),
				"select_on_edit" => true,
				"in_list" => true,
			),
		),
	),
	"downlink_tables" => array(
		"lt_catalog_items" => array(
			"key_field" => "id_lt_catalog_detail_photos",
			"name" => "name",
			"component" => "catalog_items"
		),
	),
);

?>